/*
 * Copyright (C) 2013 Sergey Zubarev, info@js-labs.org
 *
 * This file is a part of JS-MCAST framework.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.jsl.mcast;

import org.jsl.collider.ByteBufferCache;
import org.jsl.collider.RetainableByteBuffer;
import org.jsl.collider.Util;
import java.util.concurrent.atomic.AtomicInteger;
import java.net.SocketAddress;
import java.nio.ByteBuffer;

public class Test
{
    public static class StreamImpl implements Stream.Impl
    {
        private final int m_ops;
        private long m_startTime;
        private int m_packets;

        public long getPacketSN( ByteBuffer byteBuffer )
        {
            return byteBuffer.getInt();
        }

        public long handleFirstPacket( ByteBuffer byteBuffer )
        {
            return byteBuffer.getInt();
        }

        public long handlePacket( ByteBuffer byteBuffer, SocketAddress sourceAddr )
        {
            final int packetSN = byteBuffer.getInt();
            final int packets = ++m_packets;
            if (packetSN != packets)
                throw new RuntimeException( packets + "!=" + packetSN );
            if (packets == 1)
                m_startTime = System.nanoTime();
            else if (packets == m_ops)
            {
                final long endTime = System.nanoTime();
                System.out.println( Util.formatDelay( m_startTime, endTime ) + " sec." );
            }
            return (packetSN + 1);
        }

        public long handleGap( long firstLost, long firstReceived )
        {
            throw new RuntimeException();
        }

        public StreamImpl( int ops )
        {
            m_ops = ops;
        }
    }

    private static class Producer extends Thread
    {
        private final AtomicInteger m_state;
        private final int m_waitState;
        private final Stream m_stream;
        private final int m_ops;

        public Producer( AtomicInteger state, int waitState, Stream stream, int ops )
        {
            m_state = state;
            m_waitState = waitState;
            m_stream = stream;
            m_ops = ops;
        }

        public void run()
        {
            final ByteBufferCache byteBufferCache = new ByteBufferCache( true, 64*1024 );
            RetainableByteBuffer rbb = byteBufferCache.get();
            int offs = 0;

            final int threadID = m_state.getAndIncrement();
            while (m_state.get() != m_waitState);

            final long startTime = System.nanoTime();

            for (int idx=1; idx<m_ops+1; idx++)
            {
                final int space = (rbb.capacity() - offs);
                if (space < 4)
                {
                    rbb.release();
                    rbb = byteBufferCache.get();
                    offs = 0;
                }

                rbb.limit( offs + 4 );
                rbb.position( offs );
                rbb.putInt( offs, idx );
                offs += 4;

                m_stream.handleInput( rbb, null );
            }

            final long endTime = System.nanoTime();
            System.out.println( threadID + ": " + Util.formatDelay(startTime, endTime) + " sec." );

            rbb.release();
        }
    }

    public Test()
    {
    }

    public void run( Stream stream, int ops )
    {
        System.out.println( stream.getDescription() + " *** (" + ops + " ops)" );

        final AtomicInteger state = new AtomicInteger(0);
        final Producer [] producers = new Producer[2];
        for (int idx=0; idx<producers.length; idx++)
        {
            producers[idx] = new Producer( state, producers.length+1, stream, ops );
            producers[idx].start();
        }

        /* sync threads */
        while (state.get() != producers.length );
        state.incrementAndGet();

        try
        {
            for (Producer p : producers)
                p.join();
        }
        catch (InterruptedException es)
        {
            es.printStackTrace();
        }
    }


    public static void main( String [] args )
    {
        final int GAP_LIMIT = 30;
        final int WOPS = 100;
        final int OPS = 1000000;

        new Test().run( new BlockingStream("BL", GAP_LIMIT, new StreamImpl(WOPS)), WOPS );
        new Test().run( new BlockingStream("BL", GAP_LIMIT, new StreamImpl(OPS)), OPS );
        new Test().run( new LockFreeStream("LF", GAP_LIMIT, new StreamImpl(WOPS)), WOPS );
        new Test().run( new LockFreeStream("LF", GAP_LIMIT, new StreamImpl(OPS)), OPS );
    }
}
