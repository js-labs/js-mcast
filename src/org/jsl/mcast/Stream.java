/*
 * Copyright (C) 2013 Sergey Zubarev, info@js-labs.org
 *
 * This file is a part of JS-MCAST framework.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.jsl.mcast;

import org.jsl.collider.RetainableByteBuffer;
import java.net.SocketAddress;
import java.nio.ByteBuffer;

public abstract class Stream
{
    public interface Impl
    {
        long getPacketSN( ByteBuffer byteBuffer );
        long handleFirstPacket( ByteBuffer byteBuffer );
        long handlePacket( ByteBuffer byteBuffer, SocketAddress sourceAddr );
        long handleGap( long firstLost, long firstReceived );
    }

    protected final String m_description;
    protected final int m_gapLimit;

    public Stream( String description, int gapLimit )
    {
        m_description = description;
        m_gapLimit = gapLimit;
    }

    public final String getDescription()
    {
        return m_description;
    }

    public abstract void handleInput( RetainableByteBuffer data, SocketAddress sourceAddr );
}
