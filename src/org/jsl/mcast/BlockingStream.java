/*
 * Copyright (C) 2013 Sergey Zubarev, info@js-labs.org
 *
 * This file is a part of JS-MCAST framework.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.jsl.mcast;

import org.jsl.collider.RetainableByteBuffer;
import java.net.SocketAddress;
import java.util.TreeMap;
import java.util.concurrent.locks.ReentrantLock;

public class BlockingStream extends Stream
{
    private static class Node
    {
        public final RetainableByteBuffer packet;
        public final SocketAddress sourceAddr;

        public Node( RetainableByteBuffer packet, SocketAddress sourceAddr )
        {
            this.packet = packet;
            this.sourceAddr = sourceAddr;
        }
    }

    private final Impl m_impl;
    private final ReentrantLock m_lock;
    private final TreeMap<Long, Node> m_cache;
    private boolean m_handleInput;
    private long m_nextPacketSN;

    public BlockingStream( String description, int gapLimit, Impl impl )
    {
        super( description, gapLimit );
        m_impl = impl;
        m_lock = new ReentrantLock();
        m_cache = new TreeMap<Long, Node>();
        m_nextPacketSN = -1;
    }

    public void handleInput( RetainableByteBuffer data, SocketAddress sourceAddr )
    {
        final int limit = data.limit();
        final int position = data.position();
        final long packetSN = m_impl.getPacketSN( data.getByteBuffer() );
        data.limit( limit );
        data.position( position );

        m_lock.lock();
        try
        {
            if (m_handleInput || (m_nextPacketSN == -2))
            {
                if (packetSN > m_nextPacketSN)
                {
                    if (!m_cache.containsKey(packetSN))
                        m_cache.put( packetSN, new Node(data.slice(), sourceAddr) );
                }
                return;
            }
            else if (m_nextPacketSN != -1)
            {
                if (packetSN > m_nextPacketSN)
                {
                    if (!m_cache.containsKey(packetSN))
                        m_cache.put( packetSN, new Node(data.slice(), sourceAddr) );
                    return;
                }
                else if (packetSN < m_nextPacketSN)
                    return;
            }

            /* packetSN == m_nextPacketSN */
            m_handleInput = true;
        }
        finally
        {
            m_lock.unlock();
        }

        long nextPacketSN;

        if (m_nextPacketSN > 0)
        {
            assert( m_nextPacketSN == packetSN );
            nextPacketSN = m_impl.handlePacket( data.getByteBuffer(), sourceAddr );
            assert( nextPacketSN > m_nextPacketSN );
        }
        else
        {
            assert( m_nextPacketSN == -1 );

            /* first packet */
            nextPacketSN = m_impl.handleFirstPacket( data.getByteBuffer() );
            data.limit( limit );
            data.position( position );

            if (nextPacketSN > 0)
            {
                if (nextPacketSN == packetSN)
                {
                    nextPacketSN = m_impl.handlePacket( data.getByteBuffer(), sourceAddr );
                    assert( nextPacketSN > m_nextPacketSN );
                }
            }
            else
            {
                m_lock.lock();
                try
                {
                    if (m_nextPacketSN == -1)
                        m_nextPacketSN = -2;
                }
                finally
                {
                    m_lock.unlock();
                }
                return;
            }
        }

        for (;;)
        {
            Node node;

            m_lock.lock();
            try
            {
                m_nextPacketSN = nextPacketSN;
                node = m_cache.remove( m_nextPacketSN );
                if (node == null)
                {
                    m_handleInput = false;
                    break;
                }
            }
            finally
            {
                m_lock.unlock();
            }

            nextPacketSN = m_impl.handlePacket( node.packet.getByteBuffer(), node.sourceAddr );
            assert( nextPacketSN > m_nextPacketSN );
        }
    }
}
