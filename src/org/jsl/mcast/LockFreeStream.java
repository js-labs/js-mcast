/*
 * Copyright (C) 2013 Sergey Zubarev, info@js-labs.org
 *
 * This file is a part of JS-MCAST framework.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.jsl.mcast;

import org.jsl.collider.RetainableByteBuffer;

import java.net.SocketAddress;
import java.util.TreeMap;
import java.util.concurrent.atomic.AtomicReferenceFieldUpdater;
import java.util.logging.Level;
import java.util.logging.Logger;

public class LockFreeStream extends Stream
{
    private static class Node
    {
        public volatile Node next;
        public RetainableByteBuffer packet;
        public SocketAddress sourceAddr;
    }

    private void handleGap()
    {
        final long firstReceivedSN = m_cache.firstKey();
        final long nextPacketSN = m_impl.handleGap( m_nextPacketSN, firstReceivedSN );
        if (nextPacketSN > 0)
        {
            /* Skip gap */
            m_nextPacketSN = nextPacketSN;

            /* Remove all cached packets with SN < nexpPacketSN */
            for (;;)
            {
                final long sn = m_cache.firstKey();
                if (sn >= m_nextPacketSN)
                    break;

                final Node node = m_cache.remove( sn );
                assert( node.next == null );
                node.packet.release();
            }

            processCachedPackets();
            m_gapSize = 0;
        }
    }

    private void processCachedPackets()
    {
        for (;;)
        {
            final Node node = m_cache.remove( m_nextPacketSN );
            if (node == null)
                break;
            assert( node.next == null );

            final long nextPacketSN = m_impl.handlePacket( node.packet.getByteBuffer(), node.sourceAddr );
            assert( nextPacketSN > m_nextPacketSN );

            m_nextPacketSN = nextPacketSN;
            node.packet.release();
        }
    }

    private void cacheQueuedPackets( Node node )
    {
        for (;;)
        {
            Node next = node.next;
            if (next == null)
            {
                if (s_qtailUpdater.compareAndSet(this, node, m_cacheMarker))
                    break;
                while ((next = node.next) == null);
            }

            final long packetSN = m_impl.getPacketSN( node.packet.getByteBuffer() );
            if (m_cache.containsKey(packetSN))
                node.packet.release();
            else
                m_cache.put( packetSN, node );

            s_nodeNextUpdater.lazySet( node, null );
            node = next;
        }
    }

    private void processQueuedPackets( Node node )
    {
        for (;;)
        {
            Node next = node.next;
            if (next == null)
            {
                if (s_qtailUpdater.compareAndSet(this, node, null))
                    break;
                while ((next = node.next) == null);
            }

            s_nodeNextUpdater.lazySet( node, null );
            node = next;

            final int limit = node.packet.limit();
            final int position = node.packet.position();
            final long packetSN = m_impl.getPacketSN( node.packet.getByteBuffer() );
            node.packet.limit( limit );
            node.packet.position( position );

            if (packetSN == m_nextPacketSN)
            {
                long nextPacketSN = m_impl.handlePacket( node.packet.getByteBuffer(), node.sourceAddr );
                node.packet.release();
                assert( nextPacketSN > m_nextPacketSN );
                m_nextPacketSN = nextPacketSN;
                processCachedPackets();
                m_gapSize = 0;
            }
            else if (packetSN > m_nextPacketSN)
            {
                if (m_cache.containsKey(packetSN))
                    node.packet.release();
                else
                    m_cache.put( packetSN, node );

                if ((m_gapSize < m_gapLimit) && (++m_gapSize == m_gapLimit))
                    handleGap();
                else
                    processQueuedPackets( node );
            }
            else
            {
                /* packetSN < m_nextPacketSN */
                node.packet.release();
            }
        }
    }

    private static final AtomicReferenceFieldUpdater<Node, Node> s_nodeNextUpdater =
            AtomicReferenceFieldUpdater.newUpdater( Node.class, Node.class, "next" );

    private static final AtomicReferenceFieldUpdater<LockFreeStream, Node> s_qtailUpdater =
            AtomicReferenceFieldUpdater.newUpdater( LockFreeStream.class, Node.class, "m_qtail" );

    private static final Logger s_logger = Logger.getLogger( "org.jsl.mcast.LockFreeStream" );

    private final Impl m_impl;
    private final Node m_firstPacketMarker;
    private final Node m_cacheMarker;

    private volatile Node m_qtail;
    private long m_nextPacketSN;
    private int m_gapSize;
    private final TreeMap<Long, Node> m_cache;

    public LockFreeStream( String description, int gapLimit, Impl impl )
    {
        super( description, gapLimit );
        m_impl = impl;
        m_firstPacketMarker = new Node();
        m_cacheMarker = new Node();
        m_qtail = m_firstPacketMarker;
        m_cache = new TreeMap<Long, Node>();
    }

    public void handleInput( RetainableByteBuffer data, SocketAddress sourceAddr )
    {
        int limit = data.limit();
        int pos = data.position();
        final long packetSN = m_impl.getPacketSN( data.getByteBuffer() );
        data.limit( limit );
        data.position( pos );

        if (s_logger.isLoggable(Level.FINER))
            s_logger.finer( "handleInput: SN=" + packetSN + "(len=" + (limit-pos) + ")" );

        Node node = new Node();
        Node qtail = m_qtail;
        for (;;)
        {
            if (s_qtailUpdater.compareAndSet(this, qtail, node))
                break;
            qtail = m_qtail;
        }

        if (qtail == null)
        {
            if (packetSN == m_nextPacketSN)
            {
                final long nextPacketSN = m_impl.handlePacket( data.getByteBuffer(), sourceAddr );
                assert( nextPacketSN > m_nextPacketSN );
                m_nextPacketSN = nextPacketSN;
                processCachedPackets();
                m_gapSize = 0;
                processQueuedPackets( node );
            }
            else if (packetSN > m_nextPacketSN)
            {
                if (!m_cache.containsKey(packetSN))
                {
                    node.packet = data.slice();
                    node.sourceAddr = sourceAddr;
                    m_cache.put( packetSN, node );
                }

                if ((m_gapSize < m_gapLimit) && (++m_gapSize == m_gapLimit))
                    handleGap();
                else
                    processQueuedPackets( node );
            }
            else
            {
                /* packetSN < m_nextPacketSN, skip it. */
                processQueuedPackets( node );
            }
        }
        else if (qtail == m_cacheMarker)
        {
            assert( m_cacheMarker.next == null );
            if (!m_cache.containsKey(packetSN))
            {
                node.packet = data.slice();
                node.sourceAddr = sourceAddr;
                m_cache.put( packetSN, node );
            }
            cacheQueuedPackets( node );
        }
        else if (qtail == m_firstPacketMarker)
        {
            long nextPacketSN = m_impl.handleFirstPacket( data.getByteBuffer() );
            data.limit( limit );
            data.position( pos );

            if (nextPacketSN > 0)
            {
                m_nextPacketSN = nextPacketSN;
                if (packetSN == m_nextPacketSN)
                {
                    nextPacketSN = m_impl.handlePacket( data.getByteBuffer(), sourceAddr );
                    assert( nextPacketSN > m_nextPacketSN );
                    m_nextPacketSN = nextPacketSN;
                }
                processQueuedPackets( node );
            }
            else
            {
                /* We have to cache this and all further packets */
                node.packet = data.slice();
                node.sourceAddr = sourceAddr;
                m_cache.put( packetSN, node );
                cacheQueuedPackets( node );
            }
        }
        else
        {
            node.packet = data.slice();
            node.sourceAddr = sourceAddr;
            qtail.next = node;
        }
    }
}
